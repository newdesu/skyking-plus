// ==UserScript==
// @name        Skyking Plus
// @namespace   skyking
// @description An improved interface for Twente's WebSDR
// @include     http://websdr.ewi.utwente.nl:8901/
// @version     1.1
// @grant       GM_addStyle
// ==/UserScript==

/*
 * Skyking Plus
 * by New Desu
 * https://gitlab.com/newdesu/skyking-plus
*/

//new namespace wrapper for content
var skyking = {};

skyking.css = '.toggle,input[type=text]{text-shadow:1px 1px 1px #080808;box-shadow:1px 1px 1px #080808}#mainControlsWrapper,#tabsWrapper{overflow:hidden;position:fixed;bottom:0}body{background:#111;color:#ccc;padding:0;margin:0}body,body *{font-size:13px;letter-spacing:-1px;font-family:"Lucida Sans Unicode","Lucida Grande",sans-serif;font-weight:400}a{color:#75a4e0!important}.tabs label,button,input[type=button]{background:#333;color:#ccc;text-shadow:1px 1px 1px #080808;box-shadow:1px 1px 1px #080808;border:1px solid #080808}hr{opacity:0}font[color=red]{color:#fff}select{background:#333;border:1px solid #111;color:rgba(255,255,255,.8)}input[type=text]{background:#000;color:rgba(255,255,255,.8);font-family:monospace!important;font-size:1em;padding:.25em;border:1px solid #222}.modebutton{min-width:10px}header h1,header h2{display:inline}header h1{padding-right:2em}header h2{font-weight:400;color:#989898}#please{display:block;padding:.5em}#html5choice span{color:rgba(255,255,255,.6)!important}.toggle{color:#75a4e0!important;cursor:pointer;padding:.2em .5em;margin-left:1em;display:inline-block;background:#222;border:1px solid #080808}#chatboxspan b,#chatboxspan font,#chatboxspan span,#tabsWrapper .tabs .tab-0,#tabsWrapper .tabs .tab-7,#tabsWrapper .tabs label[for=tab-0],#tabsWrapper .tabs label[for=tab-7]{display:none}.toggle:hover{background:#000}#contentContainer{position:fixed;top:50%;left:50%;transform:translate(-50%,-50%);max-height:calc(100% - 200px);z-index:10000;background:#333;padding:1em;overflow-y:scroll;border-radius:.5em;box-shadow:5px 5px 5px #000;display:none}#contentContainer .mainspan{background:0 0}#soundappletdiv div{background:0 0!important}#tabsWrapper{height:58%;width:43%;left:0;background:#111}#tabsWrapper .tabs label{border-radius:0}#tabsWrapper .tabs .tab{background:0 0;border:0}#memories table{color:rgba(0,0,0,.8)}#memories table .membutton{height:auto;line-height:1.2em}#chatbox{background-color:rgba(0,0,0,1)!important;color:rgba(255,255,255,.5)!important;height:200px!important}#chatboxspan span#chatboxchallenge{display:inline}#users{height:200px;overflow-y:scroll}div.tabs div.tab:nth-of-type(5) iframe,div.tabs div.tab:nth-of-type(6) iframe{height:200px!important}div.tab{border:1px solid rgba(255,255,255,.1)}div.tabs [type=radio]:checked+label{background:#3e3e3e}div.tabs label:hover{background:#5f5d5d}div.tabs label{background:#252525;cursor:pointer}div.tabs label[style="background: rgb(204, 255, 255) none repeat scroll 0% 0%;"],div.tabs label[style="background: rgb(204, 255, 255);"]{background:#8e2c2c!important}#mainControlsWrapper{width:55%;height:58%;background:#111;z-index:6000;right:0}#advancedControls,#mainControlsWrapper .ctl{display:block;background:0 0;margin:0 .3em .3em 0;padding:.5em}.controls-panel form[name=viewform]{display:block;margin-bottom:.5em;padding:.5em 0;border-top:1px solid #080808;border-bottom:1px solid #080808}.controls-panel form[name=viewform] .ctl{display:inline!important}.controls-panel #html5choice{position:relative!important}.ctl.ctl-3,.ctl.ctl-4,.ctl.ctl-5{float:left;height:100px}.ctl.ctl-6{display:none!important}.ctl.ctl-10,.ctl.ctl-7,.ctl.ctl-8,.ctl.ctl-9{border-right:1px solid #080808;float:left;height:150px}.ctl.ctl-11,.ctl.ctl-12,.ctl.ctl-13,.ctl.ctl-14{float:left;min-height:50px}input[type=range]{width:80px!important}.keylist{background-color:#333;border:1px solid #000}#advancedControls{float:left;width:auto}input[name=frequency]{font-size:15px!important}form[name=viewform]{display:block;clear:both}form[name=viewform] #html5choice{float:none!important}form[name=viewform] br{display:none}.panel .controls-panel{float:none;clear:both;margin:0;position:relative;overflow:auto}#controlsLeftPanel{padding:.5em .5em 0}#controlsRightPanel{padding:0 .5em .5em}button[style="background: rgb(170, 170, 170);"],button[style="background: rgb(170, 170, 170) none repeat scroll 0% 0%;"]{background:#8e2c2c!important}#waterfallPanel{width:100%}#waterfallPanel #maincontrols{background:0 0}#maincontrols{text-align:center}#maincontrols #wfccontainer{display:inline-block;position:relative;margin-top:4%}#maincontrols #wfccontainer.full-width{display:block;margin:0;height:auto}#maincontrols #wfcdiv0{text-align:left}@media (max-height:1200px) and (max-width:900px){#mainControlsWrapper,#tabsWrapper{position:relative;height:auto;width:100%}}@media (max-width:1600px){#mainControlsWrapper{overflow-y:scroll}}';
GM_addStyle(skyking.css);

skyking.wrapPage = function(e) {
  //loop through child elements of the document body and append them to an element
  while(document.body.firstChild) {
      e.appendChild(document.body.firstChild);
  }
};
skyking.addCSS = function(str) {
  GM_addStyle(str);
};
skyking.waterfall = function() {
  //set up the waterfall panel
  var waterfallPanel = document.createElement("div");
  waterfallPanel.id = "waterfallPanel";
  waterfallPanel.appendChild(document.getElementById("maincontrols"));
  document.body.appendChild(waterfallPanel);

  //create a mutation observer for the style attribute of the wfccontainer...
  //this is to capture when the waterfall changes from normal to full width and add a selector without hijacking native js
  //this is a weird approach isn't it?
  var MutationObserver = window.MutationObserver || window.WebKitMutationObserver || window.MozMutationObserver;
  var element = document.getElementById("wfccontainer");//container for the waterfall that expands and contracts
  element.classList.remove("clear"); //remove the existing class which seems to do nothing
  var observer = new MutationObserver(function(mutations) {
        mutations.forEach(function(mutation) {
            targetElement = mutation.target;
            if (mutation.type == "attributes" && mutation.attributeName == "style") {//if the mutation is an attribute and the attribute is style
               if(targetElement.getAttribute("style")) {//if style has content (eg: a calcluated height for full width display)
                   targetElement.classList.add("full-width");//add the class "full-width"
               } else {//if style is empty
                   targetElement.classList.remove("full-width");//remove the class "full-width"
               }
            }
     });
  });
  observer.observe(element, {
    attributes: true 
  });

};

skyking.numberElements = function(array, uniqueIdentifier) {
  var index = 0;
  for(index = 0; index < array.length; index++) {
    array[index].className += " " + uniqueIdentifier + "-" + index;
  }
};

skyking.toggle = function(targetID) {
  var targetElement = document.getElementById(targetID);
  if(targetElement.style.display == "none") {
    targetElement.style.display = "block";
  } else if(targetElement.style.display == "block") {
    targetElement.style.display = "none";
  } else {
    return;
  }
};

skyking.newHeader = function() {
  //make the toggle button that shows more content about the hardware
  var aboutToggle = document.createElement("span");
  aboutToggle.id = "aboutToggle";
  aboutToggle.className = "toggle";
  aboutToggle.setAttribute("alt", "about this site");
  aboutToggle.setAttribute("title", "about this site");
  aboutToggle.innerHTML = "[ about this site ]";
  aboutToggle.addEventListener("click", function(event) {
    event.stopPropagation();
    skyking.toggle("contentContainer");
  });
  //make the header element
  var header = document.createElement("header");
  header.id = "aboutThisPage";
  header.innerHTML = '<h1>Wide-Band WebSDR</h1><h2>VERNON | Amateur radio club ETGD PI4THT | Faculty for Electrical Engineering, Mathematics and Computer Science | University of Twente, Enschede - The Netherlands</h2>';
  header.appendChild(aboutToggle); //append the toggle button to the end of the header
  document.body.appendChild(header); //append the header to the body
};

skyking.controls = function() {

  //give a unique class to each "ctl" element"
  skyking.numberElements(document.getElementsByClassName("ctl"), "ctl");

  //give a unique class to each "tab" element
  skyking.numberElements(document.getElementsByClassName("tab"), "tab");

  //create new container divs
  var controlsWrapper = document.createElement("div");
  var controls = document.createElement("div");

  //var controlsToggle = document.createElement("div");
  var controlsLeftPanel = document.createElement("div");
  var controlsRightPanel = document.createElement("div");

  //add selectors
  controlsLeftPanel.id = "controlsLeftPanel";
  controlsLeftPanel.className ="controls-panel";
  controlsRightPanel.id = "controlsRightPanel";
  controlsRightPanel.className = "controls-panel";
  //controlsToggle.className = "toggle";
  controlsWrapper.id = "mainControlsWrapper";
  controlsWrapper.className ="panel-wrapper";
  controls.id ="mainControls";
  controls.className = "panel";

  //capture and append old controls
  //note: on the original site, the div that contains the control panels has no selector, but it does have unique inline style which will be queried
  //todo: change this if this element ever gets a real selector
  var oldControls = document.querySelectorAll('[style="white-space:nowrap;"]')[0];
  oldControls.className = "controlInnerWrapper";//give it a selector

  //left panel
  controlsLeftPanel.appendChild(oldControls); //append inner wrapper
  var viewForm = document.forms.namedItem("viewform");
  var allowKeyboard = document.getElementsByClassName("ctl-1")[0];
  var html5choice = document.getElementById("html5choice");
  controlsLeftPanel.appendChild(document.forms.namedItem("viewform"));
  viewForm.appendChild(allowKeyboard);
  viewForm.appendChild(html5choice);
  controlsLeftPanel.appendChild(viewForm);

  //right panel
  var oldWaterFallTab = document.getElementsByClassName("tabs")[0];
  var oldAdvancedControls = document.getElementsByClassName("tab-7")[0]; //capture the content from the advanced control tab
  var advancedControls = document.createElement("div");//make a wrapper for advanced controls
  advancedControls.id = "advancedControls"; //give id
  advancedControls.innerHTML = oldAdvancedControls.innerHTML; //append old advanced controls to new advanced control wrapper
  controlsRightPanel.appendChild(oldWaterFallTab.getElementsByClassName("hideblind")[0]); //append the controls found in the first tab
  controlsRightPanel.appendChild(advancedControls); //add advanced controls to the panel

  //put it all together
  controls.appendChild(controlsLeftPanel); //append the left panel
  controls.appendChild(controlsRightPanel); //append right panel
  controlsWrapper.appendChild(controls);
  //controlsWrapper.appendChild(controlsToggle); //append toggle button to wrapper
  document.body.appendChild(controlsWrapper); //append main control wrapper to body

};

skyking.tabs = function() {
  var tabsWrapper = document.createElement("div"); //create wrapper element
  var tabsPanel = document.createElement("div");
  var tabs = document.getElementsByClassName("tabs")[0];
  tabsPanel.className = "panel";
  tabsWrapper.id = "tabsWrapper";
  tabsWrapper.className = "panel-wrapper";
  tabsPanel.appendChild(document.forms.namedItem("usernameform")); //append the username form
  tabsPanel.appendChild(tabs); //append the tabs
  tabsWrapper.appendChild(tabsPanel);
  document.body.appendChild(tabsWrapper);//append wrapper to body
};

//wrap the elements in a wrapper
//to do: clean this up
var containerDivToggle = document.createElement("span");
containerDivToggle.id = "containerDivToggle";
containerDivToggle.className = "toggle";
containerDivToggle.setAttribute("alt", "close");
containerDivToggle.setAttribute("title", "close");
containerDivToggle.innerHTML = "[ X ] CLOSE";
skyking.containerDiv = document.createElement("div"); //create wrapper
skyking.containerDiv.id = "contentContainer"; //id wrapper
skyking.containerDiv.style.display = "none";
skyking.containerDiv.appendChild(containerDivToggle);//append the toggle for closing to the container div
skyking.wrapPage(skyking.containerDiv); //function that appends body children to wrapper
document.body.appendChild(skyking.containerDiv); //append wrapper to body

//set the toggle for the container div to close
containerDivToggle.addEventListener("click", function(event) {
  event.stopPropagation();
  skyking.toggle("contentContainer");
});

//add header
skyking.newHeader();
//add controls
skyking.controls();
//add waterfall
skyking.waterfall();
//add tabs
skyking.tabs();

//auto click the chatbox tab
document.getElementById("tab-chat-label").click();

//auto select a wide waterfall
skyking.wideWaterfallCheck = document.getElementById("wfwidecheckbox");
if(!skyking.wideWaterfallCheck.checked){
  skyking.wideWaterfallCheck.click();
}

//auto select medium height waterfall
skyking.mediumWaterfallCheck = document.getElementById("wf-size-100");
if(!skyking.mediumWaterfallCheck.checked){
  skyking.mediumWaterfallCheck.click();
}

//add css
//skyking.addCSS(skyking.css);

