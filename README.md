# Skyking Plus
by New Desu

## Installation
1. **YOU MUST** _remove_ any existing userstyle or userscript you have installed for http://websdr.ewi.utwente.nl:8901/
2. You need a userscript manager, if you don't already have one.
  * Firefox: [Install Greasemonkey](https://addons.mozilla.org/en-US/firefox/addon/greasemonkey/)
  * Chrome: [Install Tampermonkey](https://chrome.google.com/webstore/detail/tampermonkey/dhdgffkkebhmkfjojejmpbldmpobfkfo?hl=en)
3. Now [click here to **install Skyking Plus**](https://gitlab.com/newdesu/skyking-plus/raw/master/skyking-plus.user.js)!

## Screenshot
![Skyking Plus](skyking-plus-screenshot.png)

## About
The purpose of this userscript is to improve the interface of the WebSDR
waterfall, minimize scrolling, and provide a dark theme. I originally
created a userstyle, but soon realized that javascript was needed to tackle
many issues with the layout.

The script puts together a new layout by looping through each element and 
encasing them inside a content container. Then, elements are sometimes given new
selectors and appended again into different panel divs. The remaining 
content becomes an "about this site" popup. This is done because many elements, 
especially the opening content, lack wrappers and selectors making them 
difficult to encase and manipulate with CSS. I created this userscript over a 
weekend to see if I could do something about the layout. The Javascript is 
spaghetti garbage and needs to be cleaned up. Feel free to fork and manipulate 
as you see fit.

## Optional: Get a list of US Air Force frequencies
1. Click the "Memories" tab
2. Click "Upload CSV & Replace"
3. Upload [this CSV file](https://gitlab.com/newdesu/skyking/raw/master/usairforce-memories.csv)